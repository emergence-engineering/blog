import { Inter, Montserrat, PT_Sans, PT_Sans_Narrow } from "next/font/google";

export const ptSans = PT_Sans({
  weight: ["400", "700"],
  subsets: ["latin"],
  display: "swap",
});

export const ptSansNarrow = PT_Sans_Narrow({
  weight: ["400", "700"],
  subsets: ["latin"],
  display: "swap",
});

export const montserrat = Montserrat({
  weight: ["400", "700"],
  subsets: ["latin"],
  display: "swap",
});

export const inter = Inter({
  weight: ["400", "700"],
  subsets: ["latin"],
  display: "swap",
});
